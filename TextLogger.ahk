﻿class TextLogger
{
	static SINGLETON_INSTANTIATION_ERROR := "TextLogger class was instantiated (``new`` keyword)!"
	static SINGLETON_INSTANTIATION_ERROR_SPECIFIC := "TextLogger is a singleton – don’t instantiate it, use the class methods and properties instead, e.g. ``TextLogger.AsyncStart()``. TextLogger uses ``Input`` command, which can’t be used simultaneously with other ``Input`` commands."
	static ALREADY_ACTIVE_ERROR := "Textlogger was told to start but it’s already running!"
	static ALREADY_ACTIVE_ERROR_SPECIFIC := "Check TextLogger.active before using ``TextLogger.SyncStart()`` or ``TextLogger.AsyncStart()``."
	static INTERVAL_NOT_INTEGER_ERROR := "TextLogger tried to start with a non–integer interval!"
	static INTERVAL_NOT_INTEGER_ERROR_SPECIFIC := "Make sure you don’t change the interval property or change it to a positive integer"
	static INTERVAL_NON_POSITIVE_ERROR := "TextLogger tried to start with a non–positive interval!"
	static INTERVAL_NON_POSITIVE_ERROR_SPECIFIC := "TextLogger doesn’t support negative and 0 intervals."
	static BAD_SETBATCHLINES_SETTING_WARNING := "TextLogger warning: in order to minimize the risk of missing characters, use ``SetBatchLines -1``"
	
	static active := false
	static buffer := ""
	static userInput := ""
	static interval := 1
	static timer
	static callback
	
	__New()
	{
		throw Exception( TextLogger.SINGLETON_INSTANTIATION_ERROR, "__New", TextLogger.SINGLETON_INSTANTIATION_ERROR_SPECIFIC )
	}
	
	syncStart( param1, param2:="no change" )
	{ 
		if ( this.active ) {
			throw Exception( this.ALREADY_ACTIVE_ERROR, "TextLogger starting…", this.ALREADY_ACTIVE_ERROR_SPECIFIC )
			return
		} 
		this.callback := param1
		
		interval := this.interval
		if ( param2 != "no change" ) {
			interval := param2
		}
		if interval is not integer
		{
			throw Exception( this.INTERVAL_NOT_INTEGER_ERROR, "TextLogger starting…", this.INTERVAL_NOT_INTEGER_ERROR_SPECIFIC )
			interval := 1
		} 
		if ( interval < 1 ) {
			throw Exception( this.INTERVAL_NON_POSITIVE_ERROR, "TextLogger starting…", this.INTERVAL_NON_POSITIVE_ERROR_SPECIFIC )
			interval := 1
		}
		this.interval := interval
		
		if ( A_BatchLines != -1 ) {
			OutputDebug % this.BAD_SETBATCHLINES_SETTING_WARNING . "`n"
			FileAppend % this.BAD_SETBATCHLINES_SETTING_WARNING . "`n", *
		}
		
		if ( !this.timer ) {
			this.timer := this.onTimer.bind( this )
		}
		timer := this.timer
		SetTimer % timer, % interval
		this.active := true
		While( this.active ) {
			Input buffer, L1000 V
			this.buffer := buffer
		}
	}
	
	stop()
	{
		timer := this.timer
		SetTimer % timer, Off
		this.active := false
		Input
	}
	
	onTimer()
	{
		if ( StrLen(this.buffer) ) {
			this.userInput .= this.buffer
			f := this.callback
			%f%( this )
		}
		Input
	}
}