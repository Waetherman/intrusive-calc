IC_factorial( base ) {
	result := 1.0
	if ( base > 100 ) {
		base := 100	;TODO: incorporate the limit into the tokenizer or signal the user that factorial wasn't computed!
	} else if ( base < 1 ) {
		base := 1
	} else {
		base := Floor( base )
	}
	
	Loop, % base {
		result *= A_Index
	}
	return result
}

IC_Root( base, degree:=2 ) {
	return base ** ( 1 / degree )
}

IC_Max( params* ) {
	;MsgBox % params.maxIndex()
;MsgBox % params[1].maxIndex()
	
	max := params.maxIndex()
	result := params[1]
	Loop, % max {
		if ( params[A_Index] > result ) {
			result := params[A_Index]
		}
	}
	return result
}	

Class Calc {
	
	static CHAR_ARGUMENT_SEPARATOR := ","
	static CHAR_DIVISION := "/"
	static CHAR_MULTIPLICATION := "*"
	static CHAR_SUBSTRACTION := "-"
	static CHAR_ADDITION := "+"
	static CHAR_EXPONENTATION := "^"
	static CHAR_LEFT_PARENTHESIS := "("
	static CHAR_RIGHT_PARENTHESIS := ")"
	static CHAR_PERCENTAGE := "%"
	static CHAR_FACTORIAL := "!"

	static TOKEN_NUMBER := "number"
	static TOKEN_FUNCTION := "function"
	static TOKEN_CONSTANT := "constant"
	static TOKEN_ARGUMENT_SEPARATOR := "argument separator"
	static TOKEN_OPERATOR := "operator"
	static TOKEN_LEFT_PARENTHESIS := "left parenthesis"
	static TOKEN_RIGHT_PARENTHESIS := "right parenthesis"
	
	static UNARY_UNKNOWN := "binary or unary"
	static UNARY_TRUE := "unary"
	static UNARY_FALSE := "binary"
	
	static ASSOCIATIVITY_LEFT := "left-associative"
	static ASSOCIATIVITY_RIGHT := "right-associative"
	
	static PRECEDENSE_DICT := { UNARY:4, Calc.CHAR_FACTORIAL:4, Calc.CHAR_EXPONENTATION:3, Calc.CHAR_DIVISION: 2, Calc.CHAR_MULTIPLICATION: 2, Calc.CHAR_PERCENTAGE: 2, Calc.CHAR_ADDITION: 1, Calc.CHAR_SUBSTRACTION: 1 }
	static IS_OPERAND := { Calc.TOKEN_NUMBER:true, Calc.TOKEN_CONSTANT:true, Calc.TOKEN_FUNCTION:true, Calc.TOKEN_LEFT_PARENTHESIS:true }
	static SPECIAL_CHARS := { Calc.CHAR_ARGUMENT_SEPARATOR:true, Calc.CHAR_DIVISION:true, Calc.CHAR_MULTIPLICATION:true, Calc.CHAR_SUBSTRACTION:true, Calc.CHAR_ADDITION:true, Calc.CHAR_EXPONENTATION:true, Calc.CHAR_LEFT_PARENTHESIS:true, Calc.CHAR_RIGHT_PARENTHESIS:true, Calc.CHAR_PERCENTAGE:true, Calc.CHAR_FACTORIAL:true, " ":true }
	static CONSTANTS := { "pi": 3.1415926535897932384, "e": 2.7182818284590452353, "root2": 1.4142135623730950488, "root3": 1.7320508075688772935, "root5": 2.2360679774997896964, "phi": 1.6180339887498948482 }
	static MAX_PARAMS := 9999
	static FUNCTIONS := { "log10": "Log", "ln": "Ln", "sqrt": "Sqrt", "sin": "Sin", "cos": "Cos", "tan": "Tan", "asin": "ASin", "acos": "ACos", "atan": "ATan", "max": "IC_Max", "root": "IC_Root" }
	static FUNCTIONS_ARG_NUM_MIN := { "log10": 1, "ln": 1, "sqrt": 1, "sin": 1, "cos": 1, "tan": 1, "asin": 1, "acos": 1, "atan": 1, "max": 2, "root": 1 }
	static FUNCTIONS_ARG_NUM_MAX := { "log10": 1, "ln": 1, "sqrt": 1, "sin": 1, "cos": 1, "tan": 1, "asin": 1, "acos": 1, "atan": 1, "max": Calc.MAX_PARAMS, "root": 2 }	
	
	Class Token {
		type := -1
		associativity := Calc.ASSOCIATIVITY_LEFT
		precedense := 1
		value := "!undefined!"
		unary := Calc.UNARY_FALSE
		
		__New( type, value ) {
			t := this
			t.value := value
			if ( type ) {
				this.type := type
			} else if ( value = Calc.CHAR_ARGUMENT_SEPARATOR ) {
				this.type := Calc.TOKEN_ARGUMENT_SEPARATOR
			} else if ( value = Calc.CHAR_LEFT_PARENTHESIS ) {
				this.type := Calc.TOKEN_LEFT_PARENTHESIS
			} else if ( value = Calc.CHAR_RIGHT_PARENTHESIS ) {
				this.type := Calc.TOKEN_RIGHT_PARENTHESIS
			} else {
				this.type := Calc.TOKEN_OPERATOR
				this.precedense := Calc.PRECEDENSE_DICT[value]
				if ( value = Calc.CHAR_EXPONENTATION ) {
					this.associativity := Calc.ASSOCIATIVITY_RIGHT
				} else if ( value = Calc.CHAR_SUBSTRACTION or value = Calc.CHAR_ADDITION or value = Calc.CHAR_PERCENTAGE ) {
					this.unary := Calc.UNARY_UNKNOWN
				} else if ( value = Calc.CHAR_FACTORIAL ) {
					this.unary := Calc.UNARY_TRUE
				}
			}
		}
		
		setUnary() {
			this.unary := Calc.UNARY_TRUE
			if ( this.value != Calc.CHAR_PERCENTAGE ) {
				this.associativity := Calc.ASSOCIATIVITY_RIGHT
			}
			this.precedense := Calc.PRECEDENSE_DICT.UNARY
		}
	}
	
	tokens := [] ;initial tokens after tokenization
	rpn := [] ;tokens ordered in RPN
	
	lastValidToken := 0
	parenthesesLevel := 0
	error := ""
	result := 0
	
	;called BEFORE adding a token
	validateTokens()
	{
		if ( !this.parenthesesLevel ) {
			index := this.tokens.maxIndex()
			if ( !index ) {
				index := 0
			}
			this.lastValidToken := index + 1 ;+1 because this method is called just before a token is added
		}
	}

	tokenize( text ) {
		this.tokens := []
		this.parenthesesLevel := 0
		this.lastValidToken := 0
		depthInfo := []
		lastToken := ""
		currentToken := ""
		buffer := ""
		functionEnforced := false
		
		index := StrLen( text ) 
		
		Loop {
			if ( index > 0 ) {
				char := SubStr( text, index, 1 )
			} else {
				char := " "
			}
			
			if ( Calc.SPECIAL_CHARS[char] ) {
				if ( StrLen(buffer) ) {
					;--- buffer resolving -----------------------------------------------
					StringLower, bufferLC, buffer
					if buffer is number
					{
						currentToken := New this.Token( Calc.TOKEN_NUMBER, buffer )
					} else if ( Calc.CONSTANTS.hasKey(bufferLC) ) {
						currentToken := New this.Token( Calc.TOKEN_CONSTANT, buffer )
					} else if ( Calc.FUNCTIONS.hasKey(bufferLC) ) {
						currentToken := New this.Token( Calc.TOKEN_FUNCTION, buffer )
						if ( !lastToken or lastToken.type != Calc.TOKEN_LEFT_PARENTHESIS ) {
							return "a function name not followed by an opening parenthesis: " . buffer
						}
						functionEnforced := false
					} else return "no such function or constant: " . buffer
					
					if ( lastToken and (currentToken.type = Calc.TOKEN_NUMBER or currentToken.type = Calc.TOKEN_CONSTANT) ) {
						if ( Calc.IS_OPERAND[lastToken.type] ) {
							return "Two operands in a row: '" . buffer . "', '" . lastToken.value . "' - multiplication by juxtaposition is currently not supported"
						} else if ( lastToken.type = Calc.TOKEN_OPERATOR and lastToken.unary = Calc.UNARY_UNKNOWN ) {
							lastToken.unary := Calc.UNARY_FALSE
						}
					}
					
					this.validateTokens()
					this.tokens.push( currentToken )
					lastToken := currentToken
					buffer := ""
					;--- end of buffer resolving ---------------------------------------/
				}
				
				if ( char != " " ) {
					if ( functionEnforced ) {
						return "function expected before an opening parenthesis (because of argument separators), but '" . char . "' was found instead!"
					}
					currentToken := New this.Token( 0, char )
					lastIsEnd := !lastToken or lastToken.type = Calc.TOKEN_RIGHT_PARENTHESIS or lastToken.type = Calc.TOKEN_ARGUMENT_SEPARATOR

					if ( currentToken.type = Calc.TOKEN_OPERATOR ) {
						if ( lastIsEnd ) {
							if ( currentToken.value = Calc.CHAR_PERCENTAGE ) {
								currentToken.setUnary()
							} else if ( currentToken.unary != Calc.UNARY_TRUE ) {
								return "no operand after " . currentToken.value . " operator!"
							}
						} else if ( Calc.IS_OPERAND[lastToken.type] ) {
							if ( currentToken.value = Calc.CHAR_PERCENTAGE ) {
								currentToken.unary := Calc.UNARY_FALSE
							} else if ( currentToken.unary = Calc.UNARY_UNKNOWN ) {
								; - or + which may be unary and therefore valid beginning of the expression
								this.validateTokens()
							} else if ( currentToken.unary = Calc.UNARY_TRUE ) {
								return "an unary, left associative operator " . currentToken.value . " is followed by an operand without a binary operator in between! Juxtaposition is not allowed."
							}
						} else if ( lastToken.type = Calc.TOKEN_OPERATOR ) {
							;two operators in a row								
							errorMessage := "two operators in a row: " . currentToken.value . " " . lastToken.value
							
							if ( lastToken.unary = Calc.UNARY_FALSE ) {
								;--- current operator should be unary because previous isn't -------------
								if ( currentToken.unary = Calc.UNARY_FALSE ) {
									return errorMessage
								
								} else if ( currentToken.unary = Calc.UNARY_UNKNOWN ) {
									if ( currentToken.value = Calc.CHAR_PERCENTAGE ) {
										currentToken.setUnary()
									} else return errorMessage ; minus or plus can't be left of a binary operator
									
								} ; else if ( currentToken.associativity = ASSOCIATIVITY_RIGHT ) return errorMessage ; commented out because impossible
								;------------------------------------------------------------------------/
								
							} else if ( lastToken.unary = Calc.UNARY_UNKNOWN ) {
								;--- previous operator is in superposition state :)-----------------------
								if ( currentToken.unary = Calc.UNARY_FALSE ) {
									if ( lastToken.value = Calc.CHAR_ADDITION or lastToken.value = Calc.CHAR_SUBSTRACTION ) {
										;unary minus/plus confirmed
										lastToken.setUnary()
									} else return errorMessage
									
								} else if ( currentToken.unary = Calc.UNARY_UNKNOWN ) {
									;two operators in a row are in superposition state! :O
									if ( currentToken.value = Calc.CHAR_PERCENTAGE ) {
										; example: 100 % - 2   this could be either modulo: 100 % (-2) or percentage: (100%) - 2
										; prioritize percentage over modulo, because negative sign of modulo divisor doesn't make a lot of sense, at least not in a simple expression without variables
										currentToken.setUnary()
										lastToken.unary := Calc.UNARY_FALSE
									} else if ( lastToken.value = Calc.CHAR_PERCENTAGE ) {
										return errorMessage ; 1 - % 2 is invalid
									} else {
										;no %, only signs (- and +)
										currentToken.unary := Calc.UNARY_FALSE
										lastToken.setUnary()
									}
									
								} else { ;currentToken.unary = UNARY_TRUE
									lastToken.unary := Calc.UNARY_FALSE ; e.g. 3 ! - 2
								}
								;------------------------------------------------------------------------/
								
							} else {
								;--- lastToken is unary... -----------------------------------------------
								; ...It's not -/+ , because signs have unary property set to unknown until next token is parsed
								; so it is a left-associative unary operator followed by other operator(s)
								if ( currentToken.value = Calc.CHAR_PERCENTAGE ) {
									currentToken.setUnary() ; 1000 % !
								} else if ( currentToken.unary != Calc.UNARY_TRUE ) {
									return errorMessage
								}
								;------------------------------------------------------------------------/
							}
						}
					
					} else if ( currentToken.type = Calc.TOKEN_LEFT_PARENTHESIS ) {
						; (
						if ( this.parenthesesLevel < 1 ) {
							return "Parentheses mismatch: too many opening parentheses."
						} else {
							functionEnforced := depthInfo.pop()
							this.parenthesesLevel--
						}
						
						if ( lastIsEnd ) {
							return "Nothing after left parenthesis"
						} else if ( lastToken.type = Calc.TOKEN_OPERATOR ) {
							if ( lastToken.unary = Calc.UNARY_UNKNOWN and (lastToken.value = Calc.CHAR_SUBSTRACTION or lastToken.value = Calc.CHAR_ADDITION) ) {
								lastToken.setUnary()
							} else return "A left operand is missing for the '" . lastToken.value . "' operator!"
						}
						if ( !functionEnforced ) {
							this.validateTokens()
						}
						
					} else if ( currentToken.type = Calc.TOKEN_RIGHT_PARENTHESIS ) {
						; )
						depthInfo.push( false ) 
						this.parenthesesLevel++
						if ( lastToken ) {
							if ( Calc.IS_OPERAND[lastToken.type] ) {
								return "Multiplication by juxtaposition (a) b is not allowed!"
							} else if ( lastToken.unary = Calc.UNARY_UNKNOWN ) {
								lastToken.unary := Calc.UNARY_FALSE
							}
						}
					} else if ( currentToken.type = Calc.TOKEN_ARGUMENT_SEPARATOR ) {
						if ( !this.parenthesesLevel ) {
							return "argument separators '" . Calc.CHAR_ARGUMENT_SEPARATOR . "' are allowed only inside parentheses following a function name"
						}
						depthInfo[this.parenthesesLevel] := true
						if ( lastIsEnd ) {
							return "No argument after the argument separator '" . currentToken.value . "'"
						} else if ( lastToken.type = Calc.TOKEN_OPERATOR ) {
							if ( lastToken.unary = Calc.UNARY_UNKNOWN and (lastToken.value = Calc.CHAR_ADDITION or lastToken.value = Calc.CHAR_SUBSTRACTION) ) {
								lastToken.setUnary()
							} else {
								return "No operand before the operator '" . lastToken.value . "'"
							}
						}
					}
					
					this.tokens.push( currentToken )
					lastToken := currentToken
				}
			} else {
				buffer := char . buffer ; we're reading from right to left, but want to have the buffer written from left to right
			}
			
			if ( index < 1 ) { 
				if ( lastToken.type = Calc.TOKEN_OPERATOR ) {
					if ( lastToken.unary = Calc.UNARY_UNKNOWN and (lastToken.value = Calc.CHAR_ADDITION or lastToken.value = Calc.CHAR_SUBSTRACTION) ) {
						lastToken.setUnary()
					} else {
						return "No operand before the operator '" . lastToken.value . "' (index < 1)" . currentToken.value . "`n" . this.tokensToString()
					}
				}
				break
			} else {
				index--
			}
		}
	}


	;Shunting Yard algorithm
	;
	;converts infix notation to Reverse Polish Notation
	;
	shuntingYard() {
		output := []
		stack := []
		
		max := this.lastValidToken
		Loop, % max {
			token := this.tokens[max-A_Index+1]
		
			if ( token.type = Calc.TOKEN_NUMBER or token.type = Calc.TOKEN_CONSTANT ) {
				output.push( token )
			} else if ( token.type = Calc.TOKEN_FUNCTION ) {
				stack.push( token )
			} else if ( token.type = Calc.TOKEN_ARGUMENT_SEPARATOR ) {
				Loop {
					if ( stack.maxIndex() < 1 ) {
						return "separator misplaced or parentheses mismatched!"
					}
					token := stack[stack.maxIndex()]
					if ( token.type = Calc.TOKEN_LEFT_PARENTHESIS ) {
						break
					}
					output.push( stack.pop() )
				}
			} else if ( token.type = Calc.TOKEN_OPERATOR ) {
				if ( token.unary = Calc.UNARY_UNKNOWN ) {
					;unary state of a sign wasn't set, because no following token was created during tokenization after tokenizing a + or -
					token.setUnary()
				}
				While ( stack.maxIndex() ) {
					token2 := stack[stack.maxIndex()]
					if ( token2.type != Calc.TOKEN_OPERATOR ) {
						break
					}
					
					if ( (token.associativity = Calc.ASSOCIATIVITY_LEFT and token.precedense <= token2.precedense) or (token.associativity = Calc.ASSOCIATIVITY_RIGHT and token.precedense < token2.precedense) ) {
						output.push( stack.pop() )
					} else {
						break
					}
				}
				stack.push( token )
			} else if ( token.type = Calc.TOKEN_LEFT_PARENTHESIS ) {
				if ( stack.maxIndex() and stack[stack.maxIndex()].type = Calc.TOKEN_FUNCTION ) {
					output.push( token ) ;an opening parenthesis to indicate the end of arguments for a function
				}
				stack.push(token)
			} else if ( token.type = Calc.TOKEN_RIGHT_PARENTHESIS ) {
				Loop {
					if ( stack.maxIndex() < 1 ) {
						return "parentheses mismatched!"
					}
					token := stack.pop()
					if ( token.type = Calc.TOKEN_LEFT_PARENTHESIS ) {
						break
					}
					output.push(token)
				}
				if ( stack[stack.maxIndex()] and stack[stack.maxIndex()].type = Calc.TOKEN_FUNCTION ) {
					output.push( stack.pop() )
				}
			}
		}
		
		While ( stack.maxIndex() ) {
			token := stack.pop()
			if ( token.type = Calc.TOKEN_LEFT_PARENTHESIS or token.type = Calc.TOKEN_RIGHT_PARENTHESIS ) {
				return "parentheses mismatched!" . token.type
			}
			output.push(token)
		}
		this.rpn := output
	}
	
	evaluate() {
		stack := []
		max := this.rpn.maxIndex()
		
		Loop, % max {
			token := this.rpn[A_Index]
			if ( token.type = Calc.TOKEN_NUMBER ) {
				stack.push( token.value )
			} else if ( token.type = Calc.TOKEN_CONSTANT ) {
				name := token.value 
				StringLower, nameLC, name
				stack.push( Calc.CONSTANTS[nameLC] )
			} else if ( token.type = Calc.TOKEN_OPERATOR ) {
				if ( stack.maxIndex() < 1 or stack[stack.maxIndex()] = Calc.CHAR_LEFT_PARENTHESIS ) {
					return "no operand for " . token.value . " operator"
				}
				operand := stack.pop()
				if ( token.unary = Calc.UNARY_FALSE ) {
					if ( stack.maxIndex() < 1 or stack[stack.maxIndex()] = Calc.CHAR_LEFT_PARENTHESIS ) {
						return "only one operand for " . token.value . " binary operator"
					}
					operand1 := stack.pop()
					if ( token.value = Calc.CHAR_ADDITION ) {
						stack.push( operand1 + operand )
					} else if ( token.value = Calc.CHAR_SUBSTRACTION ) {
						stack.push( operand1 - operand )
					} else if ( token.value = Calc.CHAR_MULTIPLICATION ) {
						stack.push( operand1 * operand )
					} else if ( token.value = Calc.CHAR_DIVISION ) {
						stack.push( operand1 / operand )
					} else if ( token.value = Calc.CHAR_EXPONENTATION ) {
						stack.push( operand1 ** operand )
					} else if ( token.value = Calc.CHAR_PERCENTAGE ) {
						stack.push( Mod(Abs(operand1), operand) ) 
					}
				} else {
					if ( token.unary = Calc.UNARY_UNKNOWN ) {
						MsgBox, % "unknown unary/binary state of an operator during evaluation phase! The operator is '" . token.value . "'"
						return "unknown unary/binary state of an operator during evaluation phase! The operator is '" . token.value . "'"
					}
					
					if ( token.value = Calc.CHAR_SUBSTRACTION ) {
						stack.push( -operand )
					} else if ( token.value = Calc.CHAR_PERCENTAGE ) {
						stack.push( operand / 100 )
					} else if ( token.value = Calc.CHAR_FACTORIAL ) {
						stack.push( IC_factorial(operand) )
					}
				}
			} else if ( token.type = Calc.TOKEN_LEFT_PARENTHESIS ) {
				stack.push( Calc.CHAR_LEFT_PARENTHESIS )
			} else if ( token.type = Calc.TOKEN_FUNCTION ) {
				arguments := []
				Loop {
					if ( stack.maxIndex() < 1 ) {
						MsgBox % "no parenthesis for a function during evaluation!"
						return "no parenthesis for a function during evaluation!"
					}
					element := stack.pop()
					if ( element = Calc.CHAR_LEFT_PARENTHESIS ) {
						break
					}
					arguments.insertAt( 1, element ) ;arguments are being read in reversed order
				}
				name := token.value
				StringLower, nameLC, name
				stack.push( this.runFunction(nameLC, arguments) )
				if ( this.error ) {
					return error
				}
			}
		}
		
		if ( stack.maxIndex() > 1 ) {
			return "Too many items on stack on end of evaluation!"
		} else if ( stack.maxIndex() < 1 ) {
			return "No result on stack on end of evaluation!"
		} else {
			this.result := stack[1]
		}
	}
	
	runFunction( name, arguments ) {
		this.error := ""
		numArgs := 0
		if ( arguments.maxIndex() ) {
			numArgs := arguments.maxIndex()
		}
		if ( numArgs < Calc.FUNCTIONS_ARG_NUM_MIN[name] or numArgs > Calc.FUNCTIONS_ARG_NUM_MAX[name] ) {
			error := numArgs . "is an INCORRECT NUMBER OF ARGUMENTS for " . name . " (max is " . Calc.FUNCTIONS_ARG_NUM_MAX[name] . " and min is " . Calc.FUNCTIONS_ARG_NUM_MIN[name] . ")"
			return 0
		}
		
		nm := Calc.FUNCTIONS[name]
		result := %nm%( arguments* )

		if ( result = "" ) {
			error := "the " . name . " function didn't return a value, probably because of incorrect input"
			return 0
		}
		
		return result
	}
	
	getDetectedExpression() {
		output := ""
		max := this.tokens.maxIndex()
		Loop, % max {
			output .= this.tokens[max-A_Index+1].value
		}
		return output
	}
	
	tokensToString() {
		output1 := ""
		output2 := ""
		Loop, % this.tokens.maxIndex() {
			output1 .= this.tokens[A_Index].value . " "
			output2 .= "{type: " . this.tokens[A_Index].type . ", value: " . this.tokens[A_Index].value . "} "
		}
		return output1 . "`n" . output2
	}
	
	printTokens(tokens:="") {
		if ( tokens = "" ) {
			tokens := this.tokens
		}
		output := ""
		Loop, % tokens.maxIndex() {
			output .= tokens[A_Index].value
			if ( A_Index < tokens.maxIndex() )
				output .= " "
		}
		return output
	}
	
	printTokensReversed( trim ) {
		output := ""
		max := trim ? this.lastValidToken : this.tokens.maxIndex()
		Loop, % max {
			output .= this.tokens[max-A_Index+1].value
			if ( A_Index < max )
				output .= " "
		}
		return output
	}
	
	calculate( input ) {
		this.result := ""
		this.tokenize( input )
		
		if ( !this.lastValidToken ) {
			return ""
		}
		error := this.shuntingYard()
		if ( error ) {
			return ""
		}
		
		error := this.evaluate()
		if ( error ) {
			return ""
		}
		return this.result
	}
}