﻿#NoEnv

#Warn All
#Warn LocalSameAsGlobal, Off
 ; #MaxHotkeysPerInterval 1000
 ; #HotkeyInterval 100
#MaxMem 256
#SingleInstance force

#MaxThreadsBuffer On
SetBatchLines -1

#Include Calc.ahk
#Include TextLogger.ahk

BAD_KEYS := ["Esc", "Enter", "Home", "PGUP", "PGDN", "End", "Left", "Right", "RButton", "MButton", "LButton", "NumpadEnter"]
TYPE_RESULT_KEYS := ["^="] ;["Tab", "#v"]
COPY_RESULT_KEYS := ["^c"]
CONTROL_HOOK_KEYS := ["F12"]

calculator := New Calc
controlHooks := {}
SettingsFile := A_ScriptDir . "\Settings.ini"
buffer := ""
lastBuffer := ""
result := ""
detectedExpression := ""
lastResult := ""
OffsetX := 0 ;offset from caret position
OffsetY := 20
parserReady := true
pastingAddsEquationMark := false
autocompleteReady := false

TOOLTIP_TYPE_TIP := "[WIN]+[V] to auto-type it down"
TOOLTIP_COPY_TIP := "[CTRL]+[C] to copy to Clipboard"
tooltipTypeTipSwitch := true
tooltipCopyTipSwitch := true

TRAYTIP_RESULT_COPIED_TITLE := "Equation result copied!"
TRAYTIP_RESULT_COPIED_TEXT := "[CTRL]+[C] copied the equation result to the clipboard. Press [CTRL]+[V] to paste the result. If you do it now, the equation mark (=) will be added."
TRAYTIP_RESULT_COPIED_TIME := 10
TRAYTIP_RESULT_COPIED_OPTIONS := 1 + 16

loadIcon()
loadControlHooks()
initHotkeys()
;TrayTip, Settings, Click the tray icon to modify settings, 5, 1

;CoordMode, Caret
SetKeyDelay, 0
SendMode, Input
; fn := this.callback.bind( this )
TextLogger.syncStart( "inputCallback" )

inputCallback( loggerSingleton ) {
	global
	
	pastingAddsEquationMark := false
	buffer .= loggerSingleton.buffer
	;ToolTip % A_Tickcount " : " buffer,0,0,13
	bufferChanged()
}

loadIcon() {
	extension := SubStr( A_ScriptFullPath, -3, 4 )
	StringLower, extension, extension 
	if ( extension = ".ahk" ) {
		
		Menu, Tray, icon, icon.ico, 1, 1
	} else if ( extension = ".exe" ) {
		Menu, Tray, icon, %A_ScriptFullPath%, -159, 1
	}
}

setDefaultNumberFormat() {
	SetFormat, FloatFast, 0.15
}

InitHotkeys() {
	global
	SetFormat, Integer, D
	
	Hotkey, $^v, pasteHotkeyLabel
	
	max := BAD_KEYS.maxIndex()
	Loop, % max {
		key := BAD_KEYS[A_Index]
		Hotkey, ~*%key%, badHotkeyLabel
	}
	
	max := TYPE_RESULT_KEYS.maxIndex()
	Loop, % max {
		key := TYPE_RESULT_KEYS[A_Index]
		Hotkey, ~%key%, typeResultHotkeyLabel
	}
	
	max := COPY_RESULT_KEYS.maxIndex()
	Loop, % max {
		key := COPY_RESULT_KEYS[A_Index]
		Hotkey, ~%key%, copyResultHotkeyLabel
	}
	
	max := CONTROL_HOOK_KEYS.maxIndex()
	Loop, % max {
		key := CONTROL_HOOK_KEYS[A_Index]
		Hotkey, ~%key%, changeControlHookHotkeyLabel
	}
	
	Hotkey, ~BackSpace, backspaceLabel
	Hotkey, ~^BackSpace, badHotkeyLabel
	
	
	setDefaultNumberFormat()
	return
	;-------------------
	
	badHotkeyLabel:
		pastingAddsEquationMark := false
		bufferReset()
	return
	
	backspaceLabel:
		pastingAddsEquationMark := false
		if ( StrLen(buffer) ) {
			StringTrimRight, buffer, buffer, 1
			bufferChanged()
		}
	return
	
	typeResultHotkeyLabel:
		pastingAddsEquationMark := false
		if ( autocompleteReady ) {
			SendInput, {SPACE}= %result%
			buffer := result
			bufferChanged()
		}
	return
	
	copyResultHotkeyLabel:
		if ( result ) {
			Clipboard := result
			TrayTip, %TRAYTIP_RESULT_COPIED_TITLE%, %TRAYTIP_RESULT_COPIED_TEXT%, %TRAYTIP_RESULT_COPIED_TIME%, %TRAYTIP_RESULT_COPIED_OPTIONS%
			pastingAddsEquationMark := true
		}
	return
	
	pasteHotkeyLabel:
	if ( pastingAddsEquationMark ) {
		Send, {Space}={Space}^v
	} else {
		Send, ^v
	}
	return
	
	changeControlHookHotkeyLabel:
	    MouseGetPos, , , WhichWindow, WhichControl
		WinGet, winProcess, ProcessName, ahk_id %WhichWindow%
		WinGetClass, winClass, ahk_id %WhichWindow%
		winUniqueID := winProcess . winClass
		if ( controlHooks.HasKey(winUniqueID) ) {
			controlHooks.delete( winUniqueID )
			saveControlHooks()
		}
		else {
			controlHooks[winUniqueID] := WhichControl
			saveControlHooks()
		}
	return
}

bufferChanged() {
	global
	if ( buffer = lastBuffer ) {
		return
	}
	
	if ( parserReady ) { ;if calculator already works, then current and/or later changes will be recalculated by recursive call(s)
		parserReady := false
		lastBuffer := buffer
		
		if ( buffer = "" ) {
			bufferReset()
			parserReady := true
			return
		}
		
		result := calculator.calculate(buffer) . "" 
		result := removeTrailingZeros( result )
		tokensNum := calculator.lastValidToken
		lastResult := result		
		
		if ( calculator.lastValidToken < 3 ) {
			disableAutocomplete()
			parserReady := true
			return
		}
		
		detectedExpression := calculator.getDetectedExpression()
		enableAutocomplete()
		parserReady := true
		bufferChanged() ;recursion: check if the buffer didn't change during calculations; if it did then calculate again
	}
}

bufferReset() {
	global
	buffer := ""
	result := ""
	disableAutocomplete()
}

disableAutocomplete() {
	global
	autocompleteReady := false
	hideTooltip()
}

enableAutocomplete() {
	global
	autocompleteReady := true
	showTooltip()
}

hideTooltip() {
	global
	ToolTip,,0,0,1
}

showTooltip() {
	global
	WinGet, winProcess, ProcessName, A
	WinGetClass, winClass, A
	winUniqueID := winProcess . winClass
	if ( controlHooks.HasKey(winUniqueID) ) {
		WhichControl := controlHooks[winUniqueID]
		ControlGetPos, x, y, w, h, %WhichControl%, A
		PosX := x
		PosY := y - 20
	} else {
		PosX := (A_CaretX != "" ? A_CaretX : 0) + OffsetX
		PosY := (A_CaretY != "" ? A_CaretY : 0) + OffsetY
	}
	text := detectedExpression . " = " . result
	if ( tooltipTypeTipSwitch ) {
		text .= "`n" . TOOLTIP_TYPE_TIP
	}
	if ( tooltipCopyTipSwitch ) {
		text .= "`n" . TOOLTIP_COPY_TIP
	}
	Tooltip, %text%, PosX, PosY, 1
	;Tooltip, %detectedExpression%, 0, 100, 15
}

saveControlHooks() {	
	global
	controlHooksKeys := ""
	controlHooksValues := ""

	for k, v in controlHooks
	{
		if ( controlHooksKeys != "" ) {
			controlHooksKeys .= ","
		}
		controlHooksKeys .= URLEncode( k )
		if ( controlHooksValues != "" ) {
			controlHooksValues .= ","
		}
		controlHooksValues .= URLEncode(v)
	}

	IniWrite, %controlHooksKeys%, %settingsFile%, Positioning, Windows
	IniWrite, %controlHooksValues%, %settingsFile%, Positioning, Controls
}

loadControlHooks() {	
	global
	IniRead, controlHooksKeys, %settingsFile%, Positioning, Windows, %A_Space%
	IniRead, controlHooksValues, %settingsFile%, Positioning, Controls, %A_Space%
	
	controlHooks := {}
	if ( controlHooksKeys = "" ) {
		Return
	}
	
	keysArr := StrSplit( controlHooksKeys, "," )
	i := 1
	Loop, Parse, controlHooksValues, `,
	{
		temp := keysArr[1]
		controlHooks[URLDecode( keysArr[i] )] := URLDecode( A_LoopField )
		i++
	}
}

RemoveTrailingZeros( number ) {	
	number .= ""
	IfInString, number, ., {
		number := RTrim( number, " 0")
	}
	number := RTrim( number, ".")
	Return number
}

; Everything below is taken from Autocomplete by Uberi
; https://github.com/Uberi/Autocomplete/
;
; License of Autocomplete:
; 
; This program is provided under the 3-clause BSD license. In short, this gives you the right to modify and distribute the program as you please, as long as you make sure the notice below is accessible to the user.
;
;    Copyright (c) 2013, Anthony Zhang
;    All rights reserved.
;    
;    Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
;    
;    Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
;    Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
;    Neither the name of the <ORGANIZATION> nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
;    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

URLEncode( Text )
{
    StringReplace, Text, Text, `%, `%25, All
    FormatInteger := A_FormatInteger, FoundPos := 0
    SetFormat, IntegerFast, Hex
    While, FoundPos := RegExMatch(Text,"S)[^\w-\.~%]",Char,FoundPos + 1)
        StringReplace, Text, Text, %Char%, % "%" . SubStr("0" . SubStr(Asc(Char),3),-1), All
    SetFormat, IntegerFast, %FormatInteger%
    Return, Text
}

URLDecode( Encoded )
{
    FoundPos := 0
    While, FoundPos := InStr(Encoded,"%",False,FoundPos + 1)
    {
        Value := SubStr(Encoded,FoundPos + 1,2)
        If (Value != "25")
            StringReplace, Encoded, Encoded, `%%Value%, % Chr("0x" . Value), All
    }
    StringReplace, Encoded, Encoded, `%25, `%, All
    Return, Encoded
}