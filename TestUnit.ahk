#Include Calc.ahk

testTokenizer( input, desiredOutput ) {
	global counter
	global failCounter
	global c
	
	result := c.tokenize( input )
	counter ++
	if ( result ) {
		failCounter ++
		MsgBox, Attempt # %counter% , Fail # %failCounter%`n%input% ended with error:`n %result%
	} else {
		;result := c.printTokens()
		result := c.printTokensReversed( false )
		if ( result != desiredOutput ) {
			failCounter ++
			MsgBox, Attempt # %counter% , Fail # %failCounter%`ninput:`n'%input%'`ndesired output:`n'%desiredOutput%'`nactual result:`n'%result%'
			MsgBox % c.tokensToString()
		}	
	}
}

testTrim( input, desiredOutput ) {
	global counter
	global failCounter
	global c
	
	c.tokenize( input )
	counter ++
	if ( c.lastValidToken ) {
		result := c.printTokensReversed( true )
	} else {
		result := ""
	}
	if ( result != desiredOutput ) {
		failCounter ++
		MsgBox, Attempt # %counter% , Fail # %failCounter%`ninput:`n'%input%'`ndesired output:`n'%desiredOutput%'`nactual result:`n'%result%'
	}
}

	testShuntingYard(input, desiredOutput) {
		global counter
		global failCounter
		global c
		
		c.tokenize( input )
		counter ++
		if ( !c.lastValidToken ) {
			failCounter ++
			MsgBox, Attempt # %counter% , Fail # %failCounter%`ninput:`n'%input%'`ndesired output:`n'%desiredOutput%'`nactual result: NO VALID TOKENS TO WORK WITH
		} else {
			result := c.shuntingYard()
			if ( result ) {
				failCounter ++
				MsgBox, Attempt # %counter% , Fail # %failCounter%`n%input% ended with error:`n %result%
			} else {
				result := c.printTokens(c.rpn)
				if ( result != desiredOutput ) {
					failCounter ++
					MsgBox, Attempt # %counter% , Fail # %failCounter%`ninput:`n'%input%'`ndesired output:`n'%desiredOutput%'`nactual result:`n'%result%'
					MsgBox % c.tokensToString()
				} 	
			}
		}
	}
	
	testEvaluation( input, desiredOutput ) {
		global counter
		global failCounter
		global c
		
		c.tokenize( input )
		counter ++
		if ( !c.lastValidToken ) {
			failCounter ++
			MsgBox, Attempt # %counter% , Fail # %failCounter%`ninput:`n'%input%'`ndesired output:`n'%desiredOutput%'`nactual result: NO VALID TOKENS TO WORK WITH
		} else {
			result := c.shuntingYard()
			if ( result ) {
				failCounter ++
				MsgBox, Attempt # %counter% , Fail # %failCounter%`n%input% ended with error:`n %result%
			} else {
				result := c.evaluate()
				if ( result ) {
					failCounter ++
					MsgBox, Attempt # %counter% , Fail # %failCounter%`n%input% ended with error:`n %result%
				} else {
					result := c.result . ""
					;MsgBox % "RESULTTTT: " resulto
					if ( result != desiredOutput ) {
						failCounter ++
						MsgBox, Attempt # %counter% , Fail # %failCounter%`ninput:`n'%input%'`ndesired output:`n'%desiredOutput%'`nactual result:`n'%result%'
						;MsgBox % c.tokensToString()
					}
				} 	
			}
		}
	}

c := New Calc
counter := 0
failCounter := 0
mainSwitch := 1

if ( 0 ) {
;below shouldn't work - should output errors
	testTokenizer( "1 - %2", 	"1 - % 2" )
	testTokenizer( "1%!2",	"1 % ! 2" )
	testTokenizer( "1 - ! 2",	"1 - ! 2" )
	testTokenizer( "1 + + + 2",	"1 + + + 2" )
	testTokenizer( "1 / + + 2",	"1 / + + 2" )
	testTokenizer( "( + + 1 )",	"( + + 1 )" )
	testTokenizer( "( )",	"( )" )
	testTokenizer( "sin ( , 1 + 2 )",	"sin ( , 1 + 2 )" )
	testTokenizer( "sin ( 1 + 2 , )",	"sin ( 1 + 2 , )" )
	testTokenizer( "sin 1 , 2",	"sin 1 , 2" )
	testTokenizer( "1 , 2",	"1 , 2" )
	testTokenizer( "sin ( 1 , , 2 )",	"sin ( 1 , , 2 )" )
	testTokenizer( "1 + ( 1 , 2 )",	"1 + ( 1 , 2 )" )
	testTokenizer( "pi ( 1 + 2 )",	"pi ( 1 + 2 )" )
	testTokenizer( "( 1 + 2 ) pi",	"( 1 + 2 ) pi" )
	testTokenizer( "1 + sin + 4",	"1 + sin + 4" )
}

if ( mainSwitch and 1 ) {
;valid mathematical strings
	testTokenizer( "4-2",	"4 - 2" )
	testTokenizer( "-2", 	"- 2" )
	testTokenizer( "-2+5",	"- 2 + 5" )
	testTokenizer( "2+2+2",	"2 + 2 + 2" )
	testTokenizer( "   +2 ++ 2++2++ 2    ++    2     +2+   2",	"+ 2 + + 2 + + 2 + + 2 + + 2 + 2 + 2" )
	testTokenizer( "                  1       +         2            ",	"1 + 2" )
	testTokenizer( "1+2-3*4/5^6%7!", 	"1 + 2 - 3 * 4 / 5 ^ 6 % 7 !" )
	testTokenizer( "50000%%!%--2", 	"50000 % % ! % - - 2" )
	testTokenizer( "1%%%%%2",	"1 % % % % % 2" )
	testTokenizer( "1% ^ -2",	"1 % ^ - 2" )

	testTokenizer( "123+456",	"123 + 456" )
	testTokenizer( "1.23+4.56",	"1.23 + 4.56" )
	testTokenizer( "1.23e456 + .15",	"1.23e456 + .15" )
	testTokenizer( "1.+.1", "1. + .1" )

	testTokenizer( "-(-10)",	"- ( - 10 )" )
	testTokenizer( "(1+2)!%2",	"( 1 + 2 ) ! % 2" )	
	testTokenizer( "( ( 1 + 2 ) / 3 ) ^ 4",	"( ( 1 + 2 ) / 3 ) ^ 4" )

	testTokenizer( "sin(90)", "sin ( 90 )" )
	testTokenizer( "pi + e",	"pi + e" )
	testTokenizer( "sin( cos( 90 * pi / 180 ) )",	"sin ( cos ( 90 * pi / 180 ) )" )
	testTokenizer( "PI*(9/2)^2",		"PI * ( 9 / 2 ) ^ 2" )
	testTokenizer( "1 * -sin( Pi / 2) ",	"1 * - sin ( Pi / 2 )" )
	testTokenizer( "( 1 + 2 ) * ( 3 / 4 ) ^ ( 5 + 6 )",		"( 1 + 2 ) * ( 3 / 4 ) ^ ( 5 + 6 )" )
	testTokenizer( "3 + 4 * 2 / ( 1 - 5 ) ^ 2 ^ 3",			"3 + 4 * 2 / ( 1 - 5 ) ^ 2 ^ 3" )
	testTokenizer( "sin ( max ( 2 , 3 ) / 3 * 3.1415 )",	"sin ( max ( 2 , 3 ) / 3 * 3.1415 )" )
	testTokenizer( "34.5*(23+1.5)/2",	"34.5 * ( 23 + 1.5 ) / 2" )
	testTokenizer( "5 + ((1 + 2) * 4) - 3",		"5 + ( ( 1 + 2 ) * 4 ) - 3" )
	testTokenizer( "3/2 + 4*(12+3)",	"3 / 2 + 4 * ( 12 + 3 )" )
	testTokenizer( "((2*(6-1))/2)*4",	"( ( 2 * ( 6 - 1 ) ) / 2 ) * 4" )
	testTokenizer( "ln(2)+3^5",		"ln ( 2 ) + 3 ^ 5" )
	testTokenizer( "11 ^ -7",	"11 ^ - 7" )
	testTokenizer( "cos ( ( 1.3 + 1 ) ^ ( 1 / 3 ) ) - log10 ( -2 * 3 / -14 )",	"cos ( ( 1.3 + 1 ) ^ ( 1 / 3 ) ) - log10 ( - 2 * 3 / - 14 )" )
	testTokenizer( "1 - (-2^2) - 1",	"1 - ( - 2 ^ 2 ) - 1" )
}

if (  mainSwitch and 1 ) {
;test if tokenizer trims the biggest possible valid mathematical string
	testTrim( "10 ) + 5",	"+ 5" )
	testTrim( "1 - %2", 	"2" )
	testTrim( "1%!2",	"2" )
	testTrim( "1 - ! 2",	"2" )
	testTrim( "1 + + + 2",	"+ 2" )
	testTrim( "1 / + + 2",	"+ 2" )
	testTrim( "( + + 1 )",	"" )
	testTrim( "( )",	"" )
	testTrim( "sin ( , 1 + 2 )",	"" )
	testTrim( "sin ( 1 + 2 , )",	"" )
	testTrim( "sin 1 , 2",	"2" )
	testTrim( "1 , 2",	"2" )
	testTrim( "sin ( 1 , , 2 )",	"" )
	testTrim( "1 + ( 1 , 2 )",	"" )
	testTrim( "pi ( 1 + 2 )",	"( 1 + 2 )" )
	testTrim( "( 1 + 2 ) pi",	"pi" )
	testTrim( "1 + sin + 4",	"+ 4" )
}

if (  mainSwitch and 1 ) {
;below strings that shouldn't be trimmed (whole strings are valid)
	testTrim( "4-2",	"4 - 2" )
	testTrim( "-2", 	"- 2" )
	testTrim( "-2+5",	"- 2 + 5" )
	testTrim( "2+2+2",	"2 + 2 + 2" )
	testTrim( "   +2 ++ 2++2++ 2    ++    2     +2+   2",	"+ 2 + + 2 + + 2 + + 2 + + 2 + 2 + 2" )
	testTrim( "                  1       +         2            ",	"1 + 2" )
	testTrim( "1+2-3*4/5^6%7!", 	"1 + 2 - 3 * 4 / 5 ^ 6 % 7 !" )
	testTrim( "50000%%!%--2", 	"50000 % % ! % - - 2" )
	testTrim( "1%%%%%2",	"1 % % % % % 2" )
	testTrim( "1% ^ -2",	"1 % ^ - 2" )

	testTrim( "123+456",	"123 + 456" )
	testTrim( "1.23+4.56",	"1.23 + 4.56" )
	testTrim( "1.23e456 + .15",	"1.23e456 + .15" )
	testTrim( "1.+.1", "1. + .1" )

	testTrim( "-(-10)",	"- ( - 10 )" )
	testTrim( "(1+2)!%2",	"( 1 + 2 ) ! % 2" )	
	testTrim( "( ( 1 + 2 ) / 3 ) ^ 4",	"( ( 1 + 2 ) / 3 ) ^ 4" )

	testTrim( "sin(90)", "sin ( 90 )" )
	testTrim( "pi + e",	"pi + e" )
	testTrim( "sin( cos( 90 * pi / 180 ) )",	"sin ( cos ( 90 * pi / 180 ) )" )
	testTrim( "PI*(9/2)^2",		"PI * ( 9 / 2 ) ^ 2" )
	testTrim( "1 * -sin( Pi / 2) ",	"1 * - sin ( Pi / 2 )" )
	testTrim( "( 1 + 2 ) * ( 3 / 4 ) ^ ( 5 + 6 )",		"( 1 + 2 ) * ( 3 / 4 ) ^ ( 5 + 6 )" )
	testTrim( "3 + 4 * 2 / ( 1 - 5 ) ^ 2 ^ 3",			"3 + 4 * 2 / ( 1 - 5 ) ^ 2 ^ 3" )
	testTrim( "sin ( max ( 2 , 3 ) / 3 * 3.1415 )",	"sin ( max ( 2 , 3 ) / 3 * 3.1415 )" )
	testTrim( "34.5*(23+1.5)/2",	"34.5 * ( 23 + 1.5 ) / 2" )
	testTrim( "5 + ((1 + 2) * 4) - 3",		"5 + ( ( 1 + 2 ) * 4 ) - 3" )
	testTrim( "3/2 + 4*(12+3)",	"3 / 2 + 4 * ( 12 + 3 )" )
	testTrim( "((2*(6-1))/2)*4",	"( ( 2 * ( 6 - 1 ) ) / 2 ) * 4" )
	testTrim( "ln(2)+3^5",		"ln ( 2 ) + 3 ^ 5" )
	testTrim( "11 ^ -7",	"11 ^ - 7" )
	testTrim( "cos ( ( 1.3 + 1 ) ^ ( 1 / 3 ) ) - log10 ( -2 * 3 / -14 )",	"cos ( ( 1.3 + 1 ) ^ ( 1 / 3 ) ) - log10 ( - 2 * 3 / - 14 )" )
	testTrim( "1 - (-2^2) - 1",	"1 - ( - 2 ^ 2 ) - 1" )
	
	testTrim( "some text that is not an expression-1 + 2", "- 1 + 2" )
}

if (  mainSwitch and 1 ) {
	testShuntingYard( "50000%%!%",	"50000 % % ! %" )
	testShuntingYard( "4 / 2 / 2", "4 2 / 2 /" )
	testShuntingYard( "-1",	"1 -" )
	testShuntingYard( "-1 + 2",	"1 - 2 +" )
	testShuntingYard("2+3",    "2 3 +")
	testShuntingYard("sin(90)",    "( 90 sin")
	testShuntingYard("( ( 1  + 2 ) / 3 ) ^ 4",    "1 2 + 3 / 4 ^")
	testShuntingYard("( 1 + 2 ) * ( 3 / 4 ) ^ ( 5 + 6 )",    "1 2 + 3 4 / 5 6 + ^ *")
	testShuntingYard("3 + 4 * 2 / ( 1 - 5 ) ^ 2 ^ 3",    "3 4 2 * 1 5 - 2 3 ^ ^ / +")
	testShuntingYard("sin ( max ( 2 , 3 ) / 3 * 3.1415 )",    "( ( 2 3 max 3 / 3.1415 * sin")
	testShuntingYard("34.5*(23+1.5)/2",    "34.5 23 1.5 + * 2 /")
	testShuntingYard("5 + ((1 + 2) * 4) - 3",    "5 1 2 + 4 * + 3 -")
	testShuntingYard("3/2 + 4*(12+3)",    "3 2 / 4 12 3 + * +")
	testShuntingYard("((2*(6-1))/2)*4",    "2 6 1 - * 2 / 4 *")
	testShuntingYard("log10( 1.11 )",    "( 1.11 log10")
	testShuntingYard("ln(2)+3^5",    "( 2 ln 3 5 ^ +")
	testShuntingYard("11 ^ -7",    "11 7 - ^")
	testShuntingYard("cos ( ( 1.3 + 1 ) ^ ( 1 / 3 ) ) - log10 ( -2 * 3 / -14 )",    "( 1.3 1 + 1 3 / ^ cos ( 2 - 3 * 14 - / log10 -")
	testShuntingYard("-8 + 5",    "8 - 5 +")
	testShuntingYard("1 - (-2^2) - 1",    "1 2 - 2 ^ - 1 -")

	;constants
	testShuntingYard("sin( cos( 90 * pi / 180 ) )",    "( ( 90 pi * 180 / cos sin")
	testShuntingYard("PI*(9/2)^2",    "PI 9 2 / 2 ^ *")
	testShuntingYard("1 * -sin( Pi / 2) ",    "1 ( Pi 2 / sin - *")
	testShuntingYard( "some text that is not an expression-1 + 2", "1 - 2 +" )
	
	;unary tests:
	testShuntingYard( "-(2 + 2)",	"2 2 + -" )
	testShuntingYard( "-sin(90)",	"( 90 sin -" )
	testShuntingYard( "-pi + 2",	"pi - 2 +" )
	testShuntingYard( "1!",	"1 !" )
	testShuntingYard( "1 + 2!",	"1 2 ! +" )
	testShuntingYard( "-1! + 2!",	"1 - ! 2 ! +" ) ;maybe factorial should have higher priority?
	testShuntingYard( "1 + (2 + 3)!",	"1 2 3 + ! +" )
	testShuntingYard( "sin(90)!",	"( 90 sin !" )

	testShuntingYard( "1%",	"1 %" )
	testShuntingYard( "1 + 2%",	"1 2 % +" )
	testShuntingYard( "-1% + 2%",	"1 - % 2 % +" ) 
	testShuntingYard( "1 + (2 + 3)%",	"1 2 3 + % +" )
	testShuntingYard( "sin(90)%",	"( 90 sin %" )
}

if (  mainSwitch and 1 ) {
	testEvaluation( "3!",	"6" )
	testEvaluation( "50000%%!%",	"1.2" )
	testEvaluation( "4 / 2 / 2", "1" )
	testEvaluation( "-1",	"-1" )
	testEvaluation( "-1 + 2",	"1" )
	testEvaluation( "2+3",    "5" )
	testEvaluation( "sin(pi/2)",    "1" )
	testEvaluation( "( ( 10  + 2 ) / 3 ) ^ 2",    "16" )
	testEvaluation( "( 1 + 2 ) * ( 3 / 1.5 ) ^ ( 8/4 )",    "12" )
	testEvaluation( "3 + 4 * 8 / ( 3 - 5 ) ^ 2 ^ 2",    "5" )

	testEvaluation( "max(pi, -2, 100*e, 1.23, 3.0e4, -pi)",	"3.0e4" )
	testEvaluation( "root(4)",	"2" )
	testEvaluation( "-root(16)",	"-4" )
	testEvaluation( "root(8, 3)",	"2" )
	testEvaluation( "root(8, root(9))",	"2" )
	
	testEvaluation( "sin ( max ( 2 , 3 ) / 3 * pi )",    "0" )
	testEvaluation( "34.5*(23+1.5)/2",    "422.625" )
	testEvaluation( "5 + ((1 + 2) * 4) - 3",    "14" )
	testEvaluation( "3/2 + 4*(12+3)",    "61.5" )
	testEvaluation( "((2*(6-1))/2)*4",    "20" )
	testEvaluation( "ln(2)+3^5",    "243.693147" )
	testEvaluation( "4 ^ -3",    "0.015625" )
	testEvaluation( "cos ( ( 1.3 + 1 ) ^ ( 1 / 3 ) ) - ln ( -2 * 3 / -14 )",    "1.095468" ) 
	testEvaluation( "-8 + 5",    "-3" )
	testEvaluation( "1 - (-2^2) - 1",    "-4" )

	;constants
	testEvaluation( "sin( cos( 90 * pi / 190 ) )",    "0.082485")
	testEvaluation( "PI*(9/2)^2",    "63.617251")
	testEvaluation( "1 * -sin( Pi / 2) ",    "-1")
	testEvaluation( "max(1, pi, -2, 100*e, 1.23, 3.0e4, -pi)",	"3.0e4" )
}

MsgBox, Test finished. %failCounter%/%counter% attempts failed.